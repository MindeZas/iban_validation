Download and extract the files inside your local server (for example inside www folder of  WAMP server);

Start your local server and launch http://localhost/index.php in your browser;

You can check IBAN number either by pasting it inside input field
 

![iban_input.JPG](https://bitbucket.org/repo/baqoqoG/images/2195086493-iban_input.JPG)





or you can check the whole list of IBAN numbers from txt file

![iban-text.JPG](https://bitbucket.org/repo/baqoqoG/images/2242993628-iban-text.JPG)


txt file structure for validation:  

```
AA051245445454552117989
LT647044001231465456
LT517044077788877777
LT227044077788877777
CC051245445454552117989
```

You can check the results by either pressing link or opening iban.out.txt file
which is on same location where index.php file.

the result file looks folowing:


```

        IBAN                 VALID.     
----------------------------------------
AA051245445454552117989 -  false  
LT647044001231465456 -  true  
LT517044077788877777 -  true  
LT227044077788877777 -  false  
CC051245445454552117989 -  false  
CC051245445454552117989 -  false  
CC051245445454552117989 -  false  
MT84MALT011000012345MTLCAST001S -  true  
NO9386011117947 -  true  
```




In case you are experiencing some problem by viewing the results online, check if this link path inside the code of index.php is correct.

![iban-link-output.JPG](https://bitbucket.org/repo/baqoqoG/images/3919964360-iban-link-output.JPG)

Also you can test this validator online
http://angularlaravel.lhosting.info/iban/index.php