<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>IBAN</title>
	<link type="text/css" media="screen" rel="stylesheet" href="style.css" />
</head>
<body>
<div class="main-wrapper">
	<div class="iban-input">
	<h4> IBAN TIKRINIMAS ĮVEDANT TIESIOGIAI </h4>
	<h4> Įveskite IBAN numerį </h4>
		<form id="formIbanInput" name="formIbanInput" method="post" action="index.php"> 
			<input type="text" id="ibanInput" name="ibanInput" size="50" placeholder = "LT12 1000 0111 0100 1000" />
			<button type="submit">tikrinti</button>
		</form>
<?php
/*********************************************************************/
 $json_file = json_decode(file_get_contents("countriesChars.json"), true);
 $countries = $json_file['countries'];
 $chars = $json_file['chars'];
 $targetDir = "";
/*********************************************************************/
 function ibanSpaceRem($ibanInput){
	$ibanMaxLen = 80;
	$ibanSpcRmd = preg_replace("/[^A-Za-z0-9]/", "", $ibanInput);
	if((strlen($ibanInput)<$ibanMaxLen)||(strlen($ibanInput)<$ibanMaxLen)){
		return $ibanSpcRmd; 	
	}
 }
/*********************************************************************/
 function ibanCountryCheckJson($ibanInput,$countries){
	$ibanInput = strtoupper($ibanInput);
	if (array_key_exists (substr($ibanInput,0,2), $countries)){
		if (strlen($ibanInput) == $countries[substr($ibanInput,0,2)]){
			$result = true;
		}
		else{
			$result = false;
		}
	}	
	else{
		$result = false;
	}
	return $result;
 }
/********************************************************************/ 
 function ibanMovedCharsArray($ibanInput,$chars){
	$MovedChar = substr($ibanInput, 4) . substr($ibanInput,0,4);
	$MovedCharArray = str_split($MovedChar);
	$NewString = "";
	foreach ($MovedCharArray as $k => $v) {
		if (!is_numeric($MovedCharArray[$k]) ) {
			$MovedCharArray[$k] = $chars[$MovedCharArray[$k]];
		}
		$NewString.= $MovedCharArray[$k];
	}
	return $NewString; 
 }
/********************************************************************/ 
 function ibanMod($ibanMd) 
{ 
	$modulus = 97;
    $take = 7;     
    $mod = ''; 
    do 
    { 
        $a = (int)$mod.substr($ibanMd,0,$take); 
        $ibanMd = substr($ibanMd,$take); 
        $mod = $a % $modulus;    
    } 
    while (strlen($ibanMd)); 
    return (int)$mod; 
}
/*******************************************************************/
 if(isset($_POST["ibanInput"])&&($_POST["ibanInput"]!= null)){
	$ibanInput = ibanSpaceRem($_POST["ibanInput"]);
	if($ibanInput){
		$ibanCountryValid = ibanCountryCheckJson($ibanInput,$countries);
		if($ibanCountryValid){
			$ibanMovedChars = ibanMovedCharsArray($ibanInput,$chars);
			$md = ibanMod($ibanMovedChars);
			if ($md == 1){
				echo "<h4>IBAN: <span class = 'ibanInput'>".$_POST["ibanInput"]."</span><span class = 'valid'> TEISINGAS </span></h4><br>";
			}else{
				echo "<h4>IBAN: <span class = 'ibanInput'>".$_POST["ibanInput"]."</span><span class = 'not-valid'> NETEISINGAS  </span></h4><br>"; 
			}
		}
		else{
			echo "<h4>IBAN: <span class = 'ibanInput'>".$_POST["ibanInput"]."</span><span class = 'not-valid'>  NETEISINGAS  </span></h4><br>";
		}
	}else{
		echo "<h5> blogai įvesta, bandykite dar kartą.  </h5>";	
	}
 }
 else if(isset($_POST["ibanInput"])&&($_POST["ibanInput"]== null)){
	 echo "<h5>  blogai įvesta, bandykite dar kartą.</h5>";
 }
?>
	</div>
		<div class="iban-file">
		<h4> IBAN TIKRINIMAS NUSKAITANT FAILĄ </h4>
		<h4> Pasirinkite failą </h4>
		 <form action="index.php" method="post" enctype="multipart/form-data"> 
			<input type="file" id="ibanFile" name="ibanFile" />
			<input type="submit" value="tikrinti" name="submit">
		 </form>
		 <br>
<?php 
/********************************************************************/ 
 if(isset($_POST["submit"])&&(basename($_FILES["ibanFile"]["name"])!= null)) {

	$targetFile = $targetDir  . basename($_FILES["ibanFile"]["name"]);
	$fileType = pathinfo($targetFile,PATHINFO_EXTENSION);
	$check = filesize($_FILES["ibanFile"]["tmp_name"]);
	if($check !== false) {
		$uploadOk = 1;
	}
	if ($_FILES["ibanFile"]["size"] > 500000) {
		echo "<h4> Failas perdidelis (max 0,5Mb). </h4>";
		$uploadOk = 0;
	}
	if($fileType != "txt") {
		echo "<h4> Nuskaitomi tik txt failai. </h4>";
		$uploadOk = 0;
	}
	if ($uploadOk == 0) {
		echo "<h4> Failas nebuvo nuskaitytas. </h4>";
	} 
	else {
		if (move_uploaded_file($_FILES["ibanFile"]["tmp_name"], $targetFile)) {
			echo "<h4> Failą pežiūrėti galite paspaudę nuorodą 
				 <span class = 'file-iban-valid'>
				 <a href='http://localhost/iban/iban.out.txt' target = '_blank'>
				 ". basename( $_FILES["ibanFile"]["name"]). "</a></span></h4>";
		} else {
			echo "<h4> Failas nebuvo nuskaitytas. </h4>";
		}
	}
/**************************************************************************/	
	if($uploadOk != 0){ 
		$fileName = $targetFile;
		$inputFile = fopen($fileName, "r") or die("Unable to open input file!");
		$outputFile = fopen("iban.out.txt", "w") or die("Unable to open output file!");
		$ibanList = file($fileName,FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
		echo "<table><tr><td class = 'table-header'>IBAN</td><td class = 'table-header'>STATUS</td></tr>";
		fwrite($outputFile,"        IBAN                 VALID.     ".PHP_EOL);
		fwrite($outputFile,"----------------------------------------".PHP_EOL);
		foreach($ibanList as $iban){
			$iban = ibanSpaceRem($iban);
			$ibanCountryValid = ibanCountryCheckJson($iban,$countries);	
			if($ibanCountryValid){
				$ibanMovedChars = ibanMovedCharsArray($iban,$chars);
				$md = ibanMod($ibanMovedChars);
				if ($md == 1){
					fwrite($outputFile,$iban." -  true  ".PHP_EOL);
					echo "<tr><td class = 'iban-data'>".$iban."</td><td class = 'valid'> TRUE </td></tr>";
				}else{
					fwrite($outputFile,$iban." -  false  ".PHP_EOL);
					echo "<tr><td class = 'iban-data'>".$iban."</td><td class = 'not-valid'> FALSE </td></tr>"; 
				}
			}
			else{
				fwrite($outputFile,$iban." -  false  ".PHP_EOL);
				echo "<tr><td class = 'iban-data'>".$iban."</td><td class = 'not-valid'> FALSE </td></tr>";
			}
		}
		echo "</tr></table>";
		fclose($inputFile);
		fclose($outputFile);	
	}
 }
/********************************************************************/ 
?></div>
</div>
</body>
</html>
